global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error


section .data
line_translation: db 10 ;перевод строки
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
	syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax ;длина строки = 0
.loop:
  cmp byte [rdi + rax], 0; проверка на 0
  je .end ;если 0, то выход
  inc rax ; увеличить длину строки на 1
  jmp .loop; начать цикл снова
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length ; rax содержит length
    mov rsi, rdi ; где начало строки
	mov rdx, rax ; сколько байтов нужно записать
	mov rax, 1 ; номер системного вызова
	mov rdi, 1 ; дескриптор
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1 ; номер системного вызова
	mov rdx, 1 ; длина строки
	push rdi ; чтобы не потерять значение в rdi, записываем в стеке
	mov rdi, 1 ; файловый дескриптор
	mov rsi, rsp ; адрес символа лежит в стеке
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
     mov rdi, line_translation
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push 0
    mov rbx, 10
    mov rax, rdi
    add rsp, 7
    .count:
    xor rdx, rdx
    div rbx
    add dl, '0'
    mov dh, byte [rsp]
    add rsp, 1
    push dx
    test rax, rax
    jne .count
    mov rdi, rsp
    push rbx
    call print_string
    pop rbx
    mov rdi, rsp
    push rbx
    call string_length
    pop rbx
    add rsp, rax
    inc rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; 
    jns print_uint ; если положительное
    push rdi 
    mov rdi, '-' 
    call print_char
    pop rdi 
    neg rdi 
    jmp print_uint;


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    xor rax, rax ; зануляем аккумулятор 
    xor rcx, rcx
    .loop:
        mov r13b, byte[rdi+rcx] ; 1 символ
        mov r12b, byte[rsi+rcx] ; 2 символ
        inc rcx ; указывает на седующий символ
        cmp r13, r12 
        je .if_null ; если zf=0, то проверяем конец ли строки
        pop r13
        pop r12
        ret ; получим 0 при разных значениях
    .if_null:
        test r13, r12 ; проверяем на совпадение значений с 0 
        jnz .loop
        inc rax
        pop r13
        pop r12
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push	0		
	mov	rax, 0	
	mov	rdi, 0 ; дескриптор
	mov	rsi, rsp ; вершину стека в rsi
	mov	rdx, 1 ; длина читаемых байтов
	syscall
	pop	rax	
	ret  

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push r12
    mov r12, rdi ; call-saved регистр
    push r13
    mov r13, rsi
.loop_space:
    call read_char
    cmp rax, 0x20
    je .loop_space
    cmp rax, 0x09
    je .loop_space
    cmp rax, 0xA
    je .loop_space
.loop:
    cmp rax, 0x0
    je .skip
    cmp rax, 0x20
    je .skip
    cmp rax, 0x9
    je .skip
    cmp rax, 0xA
    je .skip
    dec r13     ; размер n+1, т.к. учитываем нуль-терминатор
    cmp r13, 0
    jbe .return
    mov byte [r12], al
    inc r12
    call read_char
    jmp .loop

.skip:
    mov byte [r12], 0   ; записываем нуль-терминатор
    pop r13          
    pop r12 
    mov rdi, [rsp]     ; получаем rdi из rsp
    call string_length
    mov rdx, rax        ; кладем размер в аккумулятор
    pop rax             ; кладем значение rdi в аккумулятор
    ret

.return         ; достаем все значения из стека
    pop r13     
    pop r12
    pop rdi
    mov rax, 0
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx 
    xor rax, rax
    xor r8, r8 
    .first:             ; читаем первую цифру
        mov r8b, byte[rdi]
        cmp r8b, '0'
        jb .no
        cmp r8b, '9'
        ja .no
        sub r8b, '0'
        mov al, r8b
        inc rdx
    .next:              ; читаем цифры, после первой
        mov r8b, byte[rdi+rdx]
        cmp r8b, '0'
        jb .ok
        cmp r8b, '9'
        ja .ok
        inc rdx
        imul rax, 0xA ; rax*10
        sub r8b, '0'
        add rax, r8
        jmp .next
    .no:                ;цифра НЕ удовлетворяет условию
        mov rdx, 0
    .ok:                ;цифра удовлетворяет условию
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne .pls
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .pls:
      jmp parse_uint
    
print_error:
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, 2 ; stderr 
    syscall
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi ; rdi - строка
    call string_length ; в rax
    pop rdi
    
    cmp rdx, rax ; rdx - длина буффера
    jbe .mini_buf
.loop:
    push word [rdi]
    pop word [rsi]  ; rsi - буффер
    inc rdi
    inc rsi
    dec rdx
    jbe .end
    jmp .loop

.mini_buf:
    xor rax, rax
.end:
    ret 
