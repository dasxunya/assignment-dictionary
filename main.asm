%include "colon.inc"
%include "words.inc"
%include "lib.inc"

global _start
extern find_word

%define buff 256

section .rodata

hello: db "Приветик, введи ключ", 10, 0
key_found: db "Совпадение успешно найдено", 10, 0
key_not_found: db "Увы, совпадений нет", 10, 0
err_message: db "Данные INVALID-ны", 10, 0
line_translation: db 10

section .text

_start:
sub rsp, buff
mov rdi, hello
call print_string

;проверим, есть ли совпадения среди ключей
;accept(size, addr)
mov rsp, buff
mov rsi, buff
mov rdi, rsp
call read_word  ;приняли адрес начала буфера в rsp и его размер ; При успехе возвращает адрес буфера в rax, длину слова в rdx.
add rsp, buff ;вернули занимаемое место
cmp rdx, buff
ja  .buff_overflow  ;перейти, если rdx больше, чем buff size к метке, возникло переполнение

push rdx ;иначе - сохранили значение длины ключа
mov rsi, string_in
mov rdi, rax
call find_word
test rax, rax
jz .not_found_fault

;ключ найден, сохранили его значение в rax
push rax
mov rdi, key_found
call print_string
pop rax

;завершаем работу, приводим все в порядок
pop rdx ;вернули длину ключа
add rax, rdx
inc rax

;вывод значения
pop rdi
add rdi, rax
call print_string 


.buff_overflow:
    mov rdi, err_message
    jmp .for_message

.not_found_fault:
    mov rdi, key_not_found
    jmp .for_message

.for_message:
    call print_error
    rax rdi, rdi
    call exit
    ret


