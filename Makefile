FLAGS=-felf64
NASM=nasm

main: main.o lib.o dict.o 
	ld -o main main.o lib.o dict.o 

%.o: %.asm
	$(NASM) $(FLAGS) -o $@ $<

begin: main.o lib.o dict.o
	ld -o program $?
	./program


