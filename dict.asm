%define SIZE 8
extern string_equals

global find_word

section .text


.find_word:
cmp rsi, 0
jz .fault
push rdi
push rsi
call string_equals ; выводит 1 -если нашли, 0 - в противном случае
pop rsi
pop rdi
cmp rax, 1
je .succes
sub rsi, 8
mov rsi, [rsi] 
jmp find_word

.fault:
    xor rax, rax ; 0 - error
    ret

.succes:
    sub rsi, SIZE
    mov rax, rsi
    ret

